################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import paramiko
from scp import SCPClient
import shutil
import sys

def get_ssh_client(server: str, port: int, user: str, login: str):
    """Open an ssh connection to a server.
        
    Attention! The first connection to the server must be secure! If this
    can not be guaranteed then do not use .AutoAddPolicy() but rather
    add the host key fingerprint manually beforehand.

    Arguments:
        server (srt): host name or IP to ssh into
        port (int): SSH port to use
        user (str): user name on the server
        login (str): either the password or path to private key
    """

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, login)
    return client

def is_remote(url: str) -> bool:
    """Check if a file path is local or remote.

    For now everything with an "@" in it is considered remote...
    """

    return True if "@" in url else False

def put_online(source: str, destination: str):
    """Either cp or scp a file, depending on the destination."""

    if source.startswith("ftp://"):
        print("[E] FTP upload not implemented. Please inquire with the developers.")
        exit(1)
    if is_remote(destination):
        put_scp(source, destination)
    else:
        put_cp(source, destination)

def put_scp(source: str, destination: str, port=22):
    """Open SSH connection and scp a file to a remote host.

    Arguments:
        source: path to local file to copy
        destination: must be host in format "user@host:path"
        port: SSH port to use. Defaults to 22.
    """

    parts = destination.split(":")
    host = parts[0].split("@")
    ssh = get_ssh_client(host[1], port, host[0], os.path.expanduser("~/.ssh/id_rsa"))
    outfile = os.path.join(parts[1], os.path.basename(source)) # host directory + file name
    with SCPClient(ssh.get_transport()) as scp:
        scp.put(source, outfile)

def put_cp(source: str, destination: str):
    """Copy a file locally."""
    shutil.copy2(source, destination)

if __name__ == "__main__":
    source = sys.argv[1]
    destination = sys.argv[2]
    put_online(source, destination)
